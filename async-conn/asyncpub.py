import pika
import logging


LOGFMT = (
    '%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
    '-35s %(lineno) -5d: %(message)s'
)
logger = logging.getLogger(__name__)


class PublishEngine:
    def __init__(self):
        self._nmsgs = 10
        self._channel = None
        self._connection = None

    def on_open(self, connection):
        logger.info("[PublishEngine] on_open")
        self._channel = self._connection.channel(self.on_channel_open)

    def on_declare(self, channel):
        logger.info("[PublishEngine] on_declare")
        while self._nmsgs > 0:
            logger.info(f"[PublishEngine] number_of_message: {self._nmsgs}")
            self._channel.basic_publish(
                exchange='',
                routing_key='orders_q',
                body=f'H{str(self._nmsgs)}',
                properties=pika.BasicProperties(
                    content_type='text/plain',
                    delivery_mode=2)
            )
            self._nmsgs -= 1
        self._connection.close()

    def on_channel_open(self, channel):
        logger.info("[PublishEngine] on_channel_open")
        args = {'x-queue-master-locator': 'random'}
        self._channel.queue_declare(
            self.on_declare,
            queue='orders_q',
            durable=True,
            arguments=args
        )

    def on_close(self, connection, reply_code, reply_message):
        logger.info(
            "[PublishEngine] on_close, code={reply_code}, msg={reply_message}")

    def run(self):
        logging.basicConfig(level=logging.DEBUG, format=LOGFMT)
        credentials = pika.PlainCredentials('paragon', 'paragon')
        parameters = pika.ConnectionParameter(
            '127.0.0.1', 15672, '/', credentials, socket_timeout=300)
        self._connection = pika.SelectConnection(
            paramters, on_open_callback=self.on_open)
        self._connection.add_on_close_callback(self.on_close)
        logger.info("[PublishEngine] run - starting the IO loop...")
        try:
            self._connection.ioloop.start()
        except KeyboardInterrupt:
            self._connection.close()


if __name__ == '__main__':
    engine = PublishEngine()
    engine.run()
